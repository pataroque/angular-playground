import {
  AfterContentChecked,
  AfterContentInit,
  AfterViewChecked,
  AfterViewInit,
  Component,
  DoCheck,
  OnChanges,
  OnInit,
  SimpleChanges
} from '@angular/core';

@Component({
  selector: 'lch-root',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.scss' ]
})
export class AppComponent implements OnChanges, OnInit, DoCheck, AfterContentInit, AfterContentChecked, AfterViewInit, AfterViewChecked {

  public ngOnChangesCounter = 0;
  public ngOnInitCounter = 0;
  public ngDoCheckCounter = 0;
  public ngAfterContentInitCounter = 0;
  public ngAfterContentCheckedCounter = 0;
  public ngAfterViewCheckedCounter = 0;
  public ngAfterViewInitCounter = 0;

  public ngOnChanges(changes: SimpleChanges): void {
    this.ngOnChangesCounter++;
    console.log(`AppComponent OnChanges called [${ this.ngOnChangesCounter }] with: ${ changes }`);
  }

  public ngOnInit(): void {
    this.ngOnInitCounter++;
    console.log(`AppComponent OnInit called [${ this.ngOnInitCounter }]`);
  }

  public ngDoCheck(): void {
    this.ngDoCheckCounter++;
    console.log(`AppComponent DoCheck called [${ this.ngDoCheckCounter }]`);
  }

  public ngAfterContentInit(): void {
    this.ngAfterContentInitCounter++;
    console.log(`AppComponent AfterContentInit called [${ this.ngAfterContentInitCounter }]`);
  }

  public ngAfterContentChecked(): void {
    this.ngAfterContentCheckedCounter++;
    console.log(`AppComponent AfterContentChecked called [${ this.ngAfterContentCheckedCounter }]`);
  }

  public ngAfterViewInit(): void {
    this.ngAfterViewInitCounter++;
    console.log(`AppComponent AfterViewInit called [${ this.ngAfterViewInitCounter }]`);
  }

  public ngAfterViewChecked(): void {
    this.ngAfterViewCheckedCounter++;
    console.log(`AppComponent AfterViewChecked called [${ this.ngAfterViewCheckedCounter }]`);
  }
}

