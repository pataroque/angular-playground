import {
  AfterContentChecked,
  AfterContentInit,
  AfterViewChecked,
  AfterViewInit,
  Component,
  DoCheck,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges
} from '@angular/core';

export class NameModel {
  first: string;
  last: string;
}

@Component({
  selector: 'lch-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss']
})
export class InputComponent implements OnChanges, OnInit, DoCheck, AfterContentInit, AfterContentChecked, AfterViewInit,
  AfterViewChecked, OnDestroy {

  public firstName: string;
  public name: NameModel = new NameModel();

  public changeLog: string[] = [];
  public previousValue: string;
  public currentValue: string;

  public ngOnChangesCounter = 0;
  public ngOnInitCounter = 0;
  public ngDoCheckCounter = 0;
  public ngAfterContentInitCounter = 0;
  public ngAfterContentCheckedCounter = 0;
  public ngAfterViewCheckedCounter = 0;
  public ngAfterViewInitCounter = 0;
  public ngOnDestroyCounter = 0;

  public ngOnChanges(changes: SimpleChanges): void {
    for (const propName in changes) {
      if (changes.hasOwnProperty(propName)) {
        const change = changes[propName];
        this.currentValue = JSON.stringify(change.currentValue);
        this.previousValue = JSON.stringify(change.previousValue);
        this.changeLog.push(`${ propName }: currentValue = ${ this.currentValue }, previousValue = ${ this.previousValue }`);
      }
    }

    this.ngOnChangesCounter++;
    console.log(`InputComponent OnChanges called [${ this.ngOnChangesCounter }] with: ${ this.currentValue }`);
  }

  public ngOnInit(): void {
    this.ngOnInitCounter++;
    console.log(`InputComponent OnInit called [${ this.ngOnInitCounter }]`);
  }

  public ngDoCheck(): void {
    this.ngDoCheckCounter++;
    console.log(`InputComponent DoCheck called [${ this.ngDoCheckCounter }]`);
  }

  public ngAfterContentInit(): void {
    this.ngAfterContentInitCounter++;
    console.log(`InputComponent AfterContentInit called [${ this.ngAfterContentInitCounter }]`);
  }

  public ngAfterContentChecked(): void {
    this.ngAfterContentCheckedCounter++;
    console.log(`InputComponent AfterContentChecked called [${ this.ngAfterContentCheckedCounter }]`);
  }

  public ngAfterViewChecked(): void {
    this.ngAfterViewCheckedCounter++;
    console.log(`InputComponent AfterViewChecked called [${ this.ngAfterViewCheckedCounter }]`);
  }


  public ngAfterViewInit(): void {
    this.ngAfterViewInitCounter++;
    console.log(`InputComponent AfterViewInit called [${ this.ngAfterViewInitCounter }]`);
  }

  public ngOnDestroy(): void {
    this.ngOnDestroyCounter++;
    console.log(`InputComponent OnDestroy called [${ this.ngOnDestroyCounter }]`);
  }

}
