import {
  AfterContentChecked,
  AfterContentInit,
  AfterViewChecked,
  AfterViewInit,
  Component,
  DoCheck,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges
} from '@angular/core';
import { NameModel } from '../input/input.component';

@Component({
  selector: 'lch-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.scss']
})
export class ChildComponent implements OnChanges, OnInit, DoCheck, AfterContentInit, AfterContentChecked, AfterViewInit,
  AfterViewChecked, OnDestroy {

  @Input() name: NameModel;
  @Input() firstName: string;

  public changeLog: string[] = [];
  public previousValue: string;
  public currentValue: string;

  public ngOnChangesCounter = 0;
  public ngOnInitCounter = 0;
  public ngDoCheckCounter = 0;
  public ngAfterContentInitCounter = 0;
  public ngAfterContentCheckedCounter = 0;
  public ngAfterViewCheckedCounter = 0;
  public ngAfterViewInitCounter = 0;
  public ngOnDestroyCounter = 0;

  ngOnChanges(changes: SimpleChanges) {
    for (const propName in changes) {
      if (changes.hasOwnProperty(propName)) {
        const change = changes[propName];
        this.currentValue = JSON.stringify(change.currentValue);
        this.previousValue = JSON.stringify(change.previousValue);
        this.changeLog.push(`${ propName }: currentValue = ${ this.currentValue }, previousValue = ${ this.previousValue }`);
      }
    }

    this.ngOnChangesCounter++;
    console.log(`ChildComponent OnChanges called [${ this.ngOnChangesCounter }] with: ${ this.currentValue }`);
  }

  reset() {
    this.changeLog = [];
  }

  public ngOnInit(): void {
    this.ngOnInitCounter++;
    console.log(`ChildComponent OnInit called [${ this.ngOnInitCounter }]`);
  }

  public ngDoCheck(): void {
    this.ngDoCheckCounter++;
    console.log(`ChildComponent DoCheck called [${ this.ngDoCheckCounter }]`);
  }

  public ngAfterContentInit(): void {
    this.ngAfterContentInitCounter++;
    console.log(`ChildComponent AfterContentInit called [${ this.ngAfterContentInitCounter }]`);
  }

  public ngAfterContentChecked(): void {
    this.ngAfterContentCheckedCounter++;
    console.log(`ChildComponent AfterContentChecked called [${ this.ngAfterContentCheckedCounter }]`);
  }

  public ngAfterViewChecked(): void {
    this.ngAfterViewCheckedCounter++;
    console.log(`ChildComponent AfterViewChecked called [${ this.ngAfterViewCheckedCounter }]`);
  }


  public ngAfterViewInit(): void {
    this.ngAfterViewInitCounter++;
    console.log(`ChildComponent AfterViewInit called [${ this.ngAfterViewInitCounter }]`);
  }

  public ngOnDestroy(): void {
    this.ngOnDestroyCounter++;
    console.log(`ChildComponent OnDestroy called [${ this.ngOnDestroyCounter }]`);
  }

}
