import {
  AfterContentChecked,
  AfterContentInit,
  AfterViewChecked,
  AfterViewInit,
  Component,
  DoCheck,
  OnDestroy,
  OnInit
} from '@angular/core';
import { Comment, CommentsService } from '../../services/comments.service';

@Component({
  selector: 'lch-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  providers: [CommentsService]
})
export class ListComponent implements OnInit, DoCheck, AfterContentInit, AfterContentChecked, AfterViewInit,
  AfterViewChecked, OnDestroy {

  public comments: Comment[] = [];
  public checkSizeProperty: boolean;

  private page = 0;
  private size = 20;

  public ngOnInitCounter = 0;
  public ngDoCheckCounter = 0;
  public ngAfterContentInitCounter = 0;
  public ngAfterContentCheckedCounter = 0;
  public ngAfterViewCheckedCounter = 0;
  public ngAfterViewInitCounter = 0;
  public ngOnDestroyCounter = 0;
  public checkSizeCounter = 0;

  constructor(public commentsService: CommentsService) {
  }

  private getData() {
    this.commentsService.get(this.page, this.size).subscribe(
      (response: Comment[]) => {
        this.comments = this.comments.concat(response);
        this.checkSizeProperty = this.checkSizeMethod();
      }
    );
  }

  public checkSizeMethod() {
    this.checkSizeCounter++;
    console.log(`checkSizeCalled [${ this.checkSizeCounter }]`);
    return this.comments.length > 1;
  }

  public ngOnInit() {
    this.getData();
    this.ngOnInitCounter++;
    console.log(`ListComponent OnInit called [${ this.ngOnInitCounter }]`);
  }

  public ngDoCheck(): void {
    this.ngDoCheckCounter++;
    console.log(`ListComponent DoCheck called [${ this.ngDoCheckCounter }]`);
  }

  public ngAfterContentInit(): void {
    this.ngAfterContentInitCounter++;
    console.log(`ListComponent AfterContentInit called [${ this.ngAfterContentInitCounter }]`);
  }

  public ngAfterContentChecked(): void {
    this.ngAfterContentCheckedCounter++;
    console.log(`ListComponent AfterContentChecked called [${ this.ngAfterContentCheckedCounter }]`);
  }

  public ngAfterViewChecked(): void {
    this.ngAfterViewCheckedCounter++;
    console.log(`ListComponent AfterViewChecked called [${ this.ngAfterViewCheckedCounter }]`);
  }


  public ngAfterViewInit(): void {
    this.ngAfterViewInitCounter++;
    console.log(`ListComponent AfterViewInit called [${ this.ngAfterViewInitCounter }]`);
  }

  public ngOnDestroy(): void {
    this.ngOnDestroyCounter++;
    console.log(`ListComponent OnDestroy called [${ this.ngOnDestroyCounter }]`);
  }

}
