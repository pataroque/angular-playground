import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputComponent } from './components/input/input.component';
import { FormsModule } from '@angular/forms';
import { ChildComponent } from './components/child/child.component';
import { ListComponent } from './components/list/list.component';

@NgModule({
  declarations: [InputComponent, ChildComponent, ListComponent],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [
    FormsModule,
    InputComponent,
  ]
})
export class SharedModule {}
