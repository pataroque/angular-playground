import { Injectable, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

export interface Comment {
  body: string;
  email: string;
  id: number;
  name: string;
  postId: number;
}

@Injectable()
export class CommentsService implements OnDestroy {

  private endpoint = 'https://jsonplaceholder.typicode.com/comments';

  constructor(private http: HttpClient) {
  }

  public get(page: number, size: number): Observable<Comment[]> {
    return this.http.get<Comment[]>(`${ this.endpoint }?_page=${ page }&_limit=${ size }`);
  }

  public ngOnDestroy(): void {
    console.log(`CommentsService OnDestroy called`);
  }
}
