import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EagerComponent } from './eager/eager.component';
import { InputComponent } from '../shared/components/input/input.component';
import { ListComponent } from '../shared/components/list/list.component';

const routes: Routes = [
  {
    path: 'eager', component: EagerComponent,
    children: [
      { path: 'input', component: InputComponent, },
      { path: 'list', component: ListComponent, }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EagerRoutingModule {
}
