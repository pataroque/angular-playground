import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EagerRoutingModule } from './eager-routing.module';
import { EagerComponent } from './eager/eager.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [ EagerComponent ],
  imports: [
    CommonModule,
    SharedModule,
    EagerRoutingModule
  ]
})
export class EagerModule {
}
