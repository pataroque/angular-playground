import {
  AfterContentChecked,
  AfterContentInit,
  AfterViewChecked,
  AfterViewInit,
  Component,
  DoCheck,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges
} from '@angular/core';

@Component({
  selector: 'lch-lazy',
  templateUrl: './lazy.component.html',
  styleUrls: [ './lazy.component.scss' ]
})
export class LazyComponent implements OnChanges, OnInit, DoCheck, AfterContentInit, AfterContentChecked, AfterViewInit,
  AfterViewChecked, OnDestroy {

  public ngOnChangesCounter = 0;
  public ngOnInitCounter = 0;
  public ngDoCheckCounter = 0;
  public ngAfterContentInitCounter = 0;
  public ngAfterContentCheckedCounter = 0;
  public ngAfterViewCheckedCounter = 0;
  public ngAfterViewInitCounter = 0;
  public ngOnDestroyCounter = 0;

  public ngOnChanges(changes: SimpleChanges): void {
    this.ngOnChangesCounter++;
    console.log(`LazyComponent OnChanges called [${ this.ngOnChangesCounter }] with: ${ changes }`);
  }

  public ngOnInit(): void {
    this.ngOnInitCounter++;
    console.log(`LazyComponent OnInit called [${ this.ngOnInitCounter }]`);
  }

  public ngDoCheck(): void {
    this.ngDoCheckCounter++;
    console.log(`LazyComponent DoCheck called [${ this.ngDoCheckCounter }]`);
  }

  public ngAfterContentInit(): void {
    this.ngAfterContentInitCounter++;
    console.log(`LazyComponent AfterContentInit called [${ this.ngAfterContentInitCounter }]`);
  }

  public ngAfterContentChecked(): void {
    this.ngAfterContentCheckedCounter++;
    console.log(`LazyComponent AfterContentChecked called [${ this.ngAfterContentCheckedCounter }]`);
  }

  public ngAfterViewChecked(): void {
    this.ngAfterViewCheckedCounter++;
    console.log(`LazyComponent AfterViewChecked called [${ this.ngAfterViewCheckedCounter }]`);
  }


  public ngAfterViewInit(): void {
    this.ngAfterViewInitCounter++;
    console.log(`LazyComponent AfterViewInit called [${ this.ngAfterViewInitCounter }]`);
  }

  public ngOnDestroy(): void {
    this.ngOnDestroyCounter++;
    console.log(`LazyComponent OnDestroy called [${ this.ngOnDestroyCounter }]`);
  }

}
