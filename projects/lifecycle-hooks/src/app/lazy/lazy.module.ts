import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LazyRoutingModule } from './lazy-routing.module';
import { LazyComponent } from './lazy/lazy.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [ LazyComponent ],
  imports: [
    CommonModule,
    SharedModule,
    LazyRoutingModule
  ]
})
export class LazyModule {}
