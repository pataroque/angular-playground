import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LazyComponent } from './lazy/lazy.component';
import { InputComponent } from '../shared/components/input/input.component';
import { ListComponent } from '../shared/components/list/list.component';

const routes: Routes = [
  {
    path: '', component: LazyComponent,
    children: [
      { path: 'input', component: InputComponent, },
      { path: 'list', component: ListComponent, }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LazyRoutingModule {}

