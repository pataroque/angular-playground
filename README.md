# AngularPlayground

This workspace was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.8.
It consists of the following projects:

## Lyfecycle Hooks

An application created to demonstrate the behaviour of angular lifecycle hooks

##### Useful Links
[Angular official lifecycle hook tutorial](https://angular.io/guide/lifecycle-hooks)
